workers = 1
threads = 1
preload = True
timeout = 0
bind = "0.0.0.0:5001"
capture_output = True
log_level = "debug"
enable_stdio_inheritance = True
ssl_version = "TLSv1_2"
accesslog = "-"
