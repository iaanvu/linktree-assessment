from json import dumps
from typing import Mapping, Optional
from uuid import UUID

from flask import make_response, Response

from domain.common.base_service import KeyValueFilter, AttributeSorter
from domain.link.link_service import LinkService

def _apply_headers() -> Mapping[str, str]:
    return {"Content-Type": "application/json"}


def create_link(body: Mapping) -> Response:
    return make_response(
        LinkService().create(body).to_json(),
        201,
        _apply_headers()
    )


def get_link(link_id: str) -> Response:
    return make_response(
        LinkService().get_by_id(UUID(link_id)).to_json(),
        200,
        _apply_headers()
    )


def list_link(user_id: Optional[str] = None, sort_by: Optional[str] = None) -> Response:
    service = LinkService()

    filter_ = KeyValueFilter({"user_id": UUID(user_id)}) if user_id else None
    sorter = AttributeSorter(attribute_name=sort_by, reverse=True) if sort_by else None

    response_body = {
        "items": service.find(filter_, sorter)
    }

    return make_response(
        dumps(response_body, default=service.aggregate_type.__json_encoder__),
        200,
        _apply_headers()
    )
