from typing import TypedDict, Tuple, Mapping, Type, Callable

from pydantic import ValidationError

from domain.common.persistence.base_persistence_strategy import AggregateNotFound


class ErrorBody(TypedDict):
    detail: str
    status: int
    title: str


# Type expected for all handlers to return. (http_body, http_statusCode)
HttpError = Tuple[ErrorBody, int]


class BaseErrorHandler:
    # Determines the HTTP status code response.
    _status: int
    _title: str

    def __call__(self, error: Exception) -> HttpError:
        print(f"error: {error}")
        response_body: ErrorBody = {
            "detail": str(error),
            "status": self._status,
            "title": self._title,
        }
        return response_body, self._status


class BadRequestErrorHandler(BaseErrorHandler):
    _status = 400
    _title = "Bad Request"


class NotFoundErrorHandler(BaseErrorHandler):
    _status = 404
    _title = "Not Found"

# Holds the mapping of domain layer exception to a corresponding error handler.
# Errors configured here that are raised will be mapped and returned to the API client.
domain_exception_to_http_exception_mapping: Mapping[
    Type[Exception], Callable[[Exception], HttpError]
] = {
    ValidationError: BadRequestErrorHandler(),
    AggregateNotFound: NotFoundErrorHandler()
}
