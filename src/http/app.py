from pathlib import Path
from sys import path
from typing import Final

from connexion import FlaskApp, App
from connexion.resolver import RelativeResolver

# Figure out the root path.
from git import Repo


repo: Final[Repo] = Repo(".", search_parent_directories=True)
PROJECT_ROOT: Path
if project_root_dir := repo.git.rev_parse("--show-superproject-working-tree"):
    PROJECT_ROOT = Path(project_root_dir)
else:
    PROJECT_ROOT = Path(repo.working_tree_dir)

# Add the paths we'll need to run tests.
path.append(str(PROJECT_ROOT.joinpath("src")))


from error_handlers import domain_exception_to_http_exception_mapping


app: App


def configure_app() -> App:
    _app = FlaskApp(__name__, specification_dir='.')
    [_app.add_error_handler(k, v) for k, v in domain_exception_to_http_exception_mapping.items()]
    _app.add_api(
        'openapi-spec.yaml',
        resolver_error=501,
        resolver=RelativeResolver("request_handlers"),
        strict_validation=True,
        validate_responses=True
    )
    _app.run(port=8000)
    return _app


def main() -> None:
    global app
    app = configure_app()


main()
