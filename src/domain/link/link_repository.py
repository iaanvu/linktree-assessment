from typing import Sequence
from uuid import UUID

from domain.common.persistence.base_repository import BaseRepository, AggregateT
from domain.link.models.link import Link


class LinkRepository(BaseRepository[Link]):
    _aggregate_type = Link

    def find_by_user_id(self, user_id: UUID) -> Sequence[Link]:
        found = [l for l in self.find() if l.user_id == user_id]
        return found
