from typing import Mapping, Type, Optional, Sequence
from uuid import UUID

from domain.link.link_repository import LinkRepository
from domain.common.base_service import BaseService, UnsupportedSortKey
from domain.link.models.link import Link


class LinkService(BaseService[Link, LinkRepository]):
    aggregate_type = Link
    _repository_type = LinkRepository

