from typing import Mapping
from uuid import UUID

from pydantic import root_validator, constr

from domain.common.models.base_aggregate import BaseAggregate
from domain.link.models.link_content.classic_link_content import ClassicLinkContent
from domain.link.models.link_content.common.link_content_factory import LinkContentFactory, LinkTypeEnum
from domain.link.models.link_content.music_link_content import MusicLinkContent
from domain.link.models.link_content.show_link_content import ShowLinkContent


class Link(BaseAggregate):
    user_id: UUID # TODO: Is validation of user_id required?
    type: LinkTypeEnum
    title: constr(max_length=144) = ""
    content: ClassicLinkContent | MusicLinkContent | ShowLinkContent

    @root_validator(pre=True)
    def _generate_content(cls, values: Mapping):
        # Generate content based on link Type
        values["content"] = LinkContentFactory().create(values["type"], values["content"])
        return values