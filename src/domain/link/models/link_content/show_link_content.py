from datetime import datetime
from enum import Enum
from typing import Optional

from pydantic import conlist, AnyHttpUrl

from domain.common.models.base_value_object import BaseValueObject
from domain.link.models.link_content.common.base_link_content import BaseLinkContent


class ShowLocation(BaseValueObject):
    venue: str
    address: Optional[str]  # TODO: add Address class if appropriate. Clients may only want to show suburb & country.
    map_url: Optional[AnyHttpUrl] = None


class ShowSaleStatusEnum(Enum):
    SOLD_OUT = "sold_out"
    ON_SALE = "on_sale"
    PENDING_SALE = "pending_sale"


class ShowSale(BaseValueObject):
    status: ShowSaleStatusEnum
    sale_url: Optional[AnyHttpUrl] = None


class Show(BaseValueObject):
    location: ShowLocation
    date: datetime
    artist: str  # TODO: add Artist class if appropriate. Clients may want to show information about artist; genre, album.
    sale: ShowSale


class ShowLinkContent(BaseLinkContent):
    shows: conlist(Show, min_items=1)  # TODO: Validate assumption that empty list of shows is invalid state.
