from domain.common.models.base_value_object import BaseValueObject


class BaseLinkContent(BaseValueObject):
    pass


LinkContent = BaseLinkContent
