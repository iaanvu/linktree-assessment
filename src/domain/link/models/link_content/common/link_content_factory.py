from enum import Enum
from typing import Mapping

from domain.common.factory import UnsupportedFactoryType
from domain.link.models.link_content.common.base_link_content import LinkContent
from domain.link.models.link_content.classic_link_content import ClassicLinkContent
from domain.link.models.link_content.music_link_content import MusicLinkContent
from domain.link.models.link_content.show_link_content import ShowLinkContent


class LinkTypeEnum(Enum):
    CLASSIC = "classic"
    MUSIC = "music"
    SHOW = "show"


class LinkContentFactory:
    def create(self, type: str, data: Mapping) -> LinkContent:
        type_enum = LinkTypeEnum[type.upper()] if hasattr(LinkTypeEnum, type.upper()) else None

        if type_enum == LinkTypeEnum.CLASSIC:
            return ClassicLinkContent(**data)
        elif type_enum == LinkTypeEnum.MUSIC:
            return MusicLinkContent(**data)
        elif type_enum == LinkTypeEnum.SHOW:
            return ShowLinkContent(**data)

        raise UnsupportedFactoryType(f"Unsupported type: {type} for factory: {self.__class__.__name__}")
