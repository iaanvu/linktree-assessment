from pydantic import AnyHttpUrl, constr

from domain.link.models.link_content.common.base_link_content import BaseLinkContent


class ClassicLinkContent(BaseLinkContent):
    url: AnyHttpUrl

