from enum import Enum

from pydantic import conlist, AnyHttpUrl

from domain.common.models.base_value_object import BaseValueObject
from domain.link.models.link_content.common.base_link_content import BaseLinkContent


class MusicPlatformTypeEnum(Enum):
    SPOTIFY = "spotify"
    APPLE_MUSIC = "apple_music"
    SOUNDCLOUD = "soundcloud"


class MusicPlatform(BaseValueObject):
    type: MusicPlatformTypeEnum
    track_url: AnyHttpUrl
    audio_player: str


class MusicLinkContent(BaseLinkContent):
    music_platforms: conlist(MusicPlatform, min_items=1)