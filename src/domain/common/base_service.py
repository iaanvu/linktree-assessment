from functools import cached_property
from typing import Protocol, Mapping, Type, TypeVar, Generic, Sequence, Optional
from uuid import UUID

from domain.common.models.base_aggregate import Aggregate
from domain.common.persistence.base_repository import Repository, Filter

AggregateT = TypeVar("AggregateT", bound=Aggregate)
RepositoryT = TypeVar("RepositoryT", bound=Repository)


class UnsupportedSortKey(Exception):
    """Raise when aggregate does not support key."""
    pass


class KeyValueFilter(Filter):
    """
    Key Value Filter will match aggregate if every Key & Value matches an aggregate.
    """

    _key_value_mapping: Mapping[str, any]

    def __init__(self, key_value_mapping: Mapping[str, any]):
        self._key_value_mapping = key_value_mapping

    def match(self, aggregate: AggregateT) -> bool:
        return all([getattr(aggregate, key, None) == value for key, value in self._key_value_mapping.items()])


class Sorter(Protocol):
    """
    Protocol for Sorter.
    """

    reverse: bool

    def sort(self, aggregates: Sequence[AggregateT]) -> Sequence[AggregateT]:
        pass


class Service(Protocol):
    """
    Protocol for Service.

    Domain Services contain methods that implement business operations on
    the aggregate and entities of the domain model, also leveraging the
    repositories as needed to load aggregate instances and persist changes.
    """

    aggregate_type: Type[AggregateT]

    def create(self, data: Mapping) -> AggregateT:
        pass

    def get_by_id(self, id_: UUID) -> AggregateT:
        pass

    def find(self, filter_: Optional[Filter], sorter: Optional[Sorter]) -> Sequence[AggregateT]:
        pass


class AttributeSorter(Sorter):
    """
    Attribute Sorter that sorts aggregates based on its attribute value.
    """

    _attribute_name: str

    def __init__(self, attribute_name: str, reverse: bool = False) -> None:
        self._attribute_name = attribute_name
        self.reverse = reverse

    def sort(self, aggregates: Sequence[AggregateT]) -> Sequence[AggregateT]:
        return sorted(aggregates, key=lambda a: getattr(a, self._attribute_name), reverse=self.reverse)


class BaseService(Generic[AggregateT, RepositoryT]):
    aggregate_type: Type[AggregateT]
    _repository_type: Type[RepositoryT]

    def create(self, data: Mapping) -> AggregateT:
        aggregate = self.aggregate_type(**data)
        self._repository.save(aggregate)
        return aggregate

    def get_by_id(self, id_: UUID) -> AggregateT:
        return self._repository.get_by_id(id_)

    def find(self, filter_: Optional[Filter] = None, sorter: Optional[Sorter] = None) -> Sequence[AggregateT]:
        aggregates = self._maybe_find_by_filter(filter_)
        return self._maybe_sort_aggregates(aggregates, sorter)

    def _maybe_find_by_filter(self, filter_: Filter) -> Sequence[AggregateT]:
        if filter_:
            return self._repository.find_by_filter(filter_)
        else:
            return self._repository.find()

    def _maybe_sort_aggregates(self, aggregates: Sequence[AggregateT], sorter: Optional[Sorter]) \
            -> Sequence[AggregateT]:
        if sorter:
            return sorter.sort(aggregates)
        else:
            return aggregates

    @cached_property
    def _repository(self) -> RepositoryT:
        return self._repository_type()
