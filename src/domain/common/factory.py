from typing import Mapping, Any, Protocol, TypeVar

objectT = AggregateT = TypeVar("ObjectT")

class UnsupportedFactoryType(Exception):
    """Raise when requesting Factory a type that is unsupported."""
    pass

class Factory(Protocol):
    """
    Protocol for Factory that will return an instance based on a type
    """

    def create(self, type_: str, data: Mapping) -> objectT:
        pass

