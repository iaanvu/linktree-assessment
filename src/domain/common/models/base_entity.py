from typing import Protocol
from uuid import UUID, uuid4
from datetime import datetime

from pydantic import Field

from domain.common.models.base_value_object import ValueObject, BaseValueObject



class Entity(ValueObject, Protocol):
    """
    Protocol for Entity.

    An Entity is a general abstraction of object with identity.

    Entity is taken from Domain Driven Design as reference object defined primarily by its identity (id).
    """

    id: UUID


class BaseEntity(BaseValueObject):
    id: UUID = Field(default_factory=uuid4)
    created_at: datetime = Field(default_factory=datetime.now)
