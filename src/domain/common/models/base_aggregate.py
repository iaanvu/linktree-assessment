from typing import Protocol

from domain.common.models.base_entity import Entity, BaseEntity

class Aggregate(Entity, Protocol):
    """
     Protocol for Aggregate.

     An aggregate is a general abstraction, but is also a container for other entities and
     value objects and forms a consistency boundary in terms of persistence.

     Aggregate in this case is actually an Aggregate Root which is taken from Domain Driven Design.
    """
    pass


class BaseAggregate(BaseEntity):
    pass