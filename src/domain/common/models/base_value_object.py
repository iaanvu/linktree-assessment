from typing import Protocol

from pydantic import BaseModel


class ValueObject(Protocol):
    """
    Protocol for Value Object.

    Value Object is taking from Domain Driven Design as object with no identity (id).
    this means two value object are considered equal if their properties and values
    are identical.
    """

    @classmethod
    def from_json(cls, json: str) -> "ValueObject":
        pass

    def to_json(self) -> str:
        pass


class BaseDomainModel(BaseModel):
    class Config:
        # Once constructed, instances cannot be modified.
        frozen = True


class BaseValueObject(BaseDomainModel):

    @classmethod
    def from_json(cls, json: str) -> "ValueObject":
        return cls.parse_raw(json)

    def to_json(self) -> str:
        return self.json()
