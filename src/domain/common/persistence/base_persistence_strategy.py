from abc import abstractmethod
from typing import Protocol, TypeVar, Type, Sequence, Optional
from uuid import UUID

from domain.common.models.base_aggregate import Aggregate

AggregateT = TypeVar("AggregateT", bound=Aggregate)


class AggregateNotFound(Exception):
    """Raise when requested aggregate is not found"""
    pass


class PersistenceStrategy(Protocol):
    """
    Protocol for Persistence Strategy.

    Persistence strategies encapsulate the storage of data.
    This applies the strategy pattern.
    """
    def save(self, aggregate: Aggregate) -> None:
        pass

    def get_by_id(self, aggregate_type: Type[AggregateT], id_: UUID) -> AggregateT:
        pass

    def find(self, aggregate_type: Type[AggregateT]) -> Sequence[AggregateT]:
        pass


class BasePersistenceStrategy:
    @abstractmethod
    def save(self, aggregate: Aggregate) -> None:
        raise NotImplementedError()

    @abstractmethod
    def get_by_id(self, aggregate_type: Type[AggregateT], id_: UUID) -> AggregateT:
        raise NotImplementedError()

    @abstractmethod
    def find(self, aggregate_type: Type[AggregateT]) -> Sequence[AggregateT]:
        raise NotImplementedError()
