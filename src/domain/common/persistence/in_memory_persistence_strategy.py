from collections import defaultdict
from typing import Mapping, Type, Optional, Sequence
from uuid import UUID

from domain.common.models.base_aggregate import Aggregate
from domain.common.persistence.base_persistence_strategy import BasePersistenceStrategy, AggregateT


class InMemoryPersistenceStrategy(BasePersistenceStrategy):
    """
    In memory persistence strategy uses a dictionary to persist aggregates.
    """
    _data: dict[Type[AggregateT], dict[UUID, Aggregate]] = defaultdict(lambda: dict())

    def save(self, aggregate: Aggregate) -> None:
        self._data[aggregate.__class__][aggregate.id] = aggregate

    def get_by_id(self, aggregate_type: Type[AggregateT], id_: UUID) -> Optional[AggregateT]:
        if id_ in self._data[aggregate_type]:
            return self._data[aggregate_type][id_]
        else:
            return None

    def find(self, aggregate_type: Type[AggregateT]) -> Sequence[AggregateT]:
        return list(self._data[aggregate_type].values())
