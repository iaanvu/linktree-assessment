from typing import Protocol, TypeVar, Type, Generic, Final, Optional, Sequence
from uuid import UUID

from domain.common.models.base_aggregate import Aggregate
from domain.common.persistence.base_persistence_strategy import PersistenceStrategy, AggregateNotFound
from domain.common.persistence.in_memory_persistence_strategy import InMemoryPersistenceStrategy

AggregateT = TypeVar("AggregateT", bound=Aggregate)


class Filter(Protocol):
    """
    Protocol for Filter.

    Filters perform a match on an aggregate, returning True if the aggregate
    is considered a match.
    """
    def match(self, aggregate: AggregateT) -> bool:
        pass


class Repository(Protocol[AggregateT]):
    """
    Protocol for Repository.

    Repositories encapsulate the logic required perform CRUD activities on Aggregates.
    """
    _aggregate_type: Type[AggregateT]

    def save(self, aggregate: Aggregate) -> None:
        pass

    def get_by_id(self, id: UUID) -> AggregateT:
        pass

    def find(self) -> Sequence[AggregateT]:
        pass

    def find_by_filter(self, filter_: Filter) -> Sequence[AggregateT]:
        pass


DEFAULT_PERSISTENCE_STRATEGY: PersistenceStrategy = InMemoryPersistenceStrategy()


class BaseRepository(Generic[AggregateT]):
    _persistence_strategy: Final[PersistenceStrategy]
    _aggregate_type: Type[AggregateT]

    def __init__(self, persistence_strategy: Optional[PersistenceStrategy] = None) -> None:
        self._persistence_strategy = persistence_strategy or DEFAULT_PERSISTENCE_STRATEGY

    def save(self, aggregate: Aggregate) -> None:
        self._persistence_strategy.save(aggregate)

    def get_by_id(self, id_: UUID) -> AggregateT:
        if aggregate := self._persistence_strategy.get_by_id(self._aggregate_type, id_):
            return aggregate
        else:
            raise AggregateNotFound(f"{self._aggregate_type.__name__} does not exist with "
                                    f"id: {id_}")

    def find(self) -> Sequence[AggregateT]:
        return self._persistence_strategy.find(self._aggregate_type)

    def find_by_filter(self, filter_: Filter) -> Sequence[AggregateT]:
        aggregate = self._persistence_strategy.find(self._aggregate_type)
        return [a for a in aggregate if filter_.match(a)]
