from pydantic import ValidationError
from pytest import raises, mark

from domain.link.models.link_content.classic_link_content import ClassicLinkContent


VALID_URLS = {
    "http://google.com",
    "http://google.com?search=hello"  # ensure query parameter is allowed
    "http://google.com?search=hello&test=true"  # ensure multiple query parameters is allowed
}

@mark.parametrize("url", VALID_URLS)
def test_can_be_created_with_valid_urls(url: str) -> None:
    assert ClassicLinkContent(url=url)


INVALID_URLS = {"google.com", "google"}

@mark.parametrize("url", INVALID_URLS)
def test_raises_with_invalid_url(url: str) -> None:
    with raises(ValidationError, match="invalid or missing URL scheme"):
        ClassicLinkContent(url=url)
