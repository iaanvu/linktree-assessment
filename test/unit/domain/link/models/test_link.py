from uuid import uuid4

from pytest import raises
from pydantic import ValidationError

from domain.common.factory import UnsupportedFactoryType
from domain.link.models.link import Link
from domain.link.models.link_content.common.link_content_factory import LinkTypeEnum


VALID_LINK_ARGS = {
    "user_id": "77da9050-54b0-491d-b358-79ffec80d6a0",
    "title": "My Link Title",
    "type": LinkTypeEnum.CLASSIC.value,
    "content": {"url": "http://google.com"}
}

def test_raises_when_title_is_too_long() -> None:
    with raises(ValidationError, match="ensure this value has at most 144 characters"):
        Link(**{**VALID_LINK_ARGS, "title": "x"*155})

def test_raises_with_unsupported_type()-> None:
    with raises(UnsupportedFactoryType):
        Link(**{**VALID_LINK_ARGS, "type": "unsupported_type"})
