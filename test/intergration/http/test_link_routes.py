from datetime import datetime
from uuid import uuid4

import requests
from pytest import mark

from domain.link.models.link_content.music_link_content import MusicPlatformTypeEnum
from domain.link.models.link_content.show_link_content import ShowSaleStatusEnum

API_BASE_URL = "http://localhost:8000"
MOCK_USER_ID = "ba8b3bbd-425f-4bef-b314-a08d5371292f"
LINK_ROUTE = "links"


VALID_POST_LINK_BODY = {
    "user_id": MOCK_USER_ID,
    "title": "Ian Vu",
    "type": "classic",
    "content": {
        "url": "http://ian.vu"
    }
}

###################################################################################################
# Sunny Tests
###################################################################################################


def test_post_link_classic_type() -> None:
    response = requests.post(f"{API_BASE_URL}/{LINK_ROUTE}", json=VALID_POST_LINK_BODY)
    assert response.status_code == 201



MUSIC_PLATFORM_TYPES = {e.value for e in MusicPlatformTypeEnum}

@mark.parametrize("platform_type", MUSIC_PLATFORM_TYPES)
def test_post_link_music_type(platform_type: str) -> None:
    response = requests.post(f"{API_BASE_URL}/{LINK_ROUTE}", json={
        **VALID_POST_LINK_BODY,
        "type": "music",
        "content": {
            "music_platforms": [
                {
                    "type": platform_type,
                    "track_url": "https://open.spotify.com/track/76Zi40dd2HoTn3sehttJhY?si=5b2c9e0fb8cf4ab6",
                    "audio_player": '<iframe src="https://open.spotify.com/embed/album/1DFixLWuPkv3KT3TnV35m3" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>'
                },
            ]
        }
    })
    assert response.status_code == 201


SALE_STATUS = {e.value for e in ShowSaleStatusEnum}

@mark.parametrize("sale_status", SALE_STATUS)
def test_post_link_show_type(sale_status: str) -> None:
    response = requests.post(f"{API_BASE_URL}/{LINK_ROUTE}", json={
        **VALID_POST_LINK_BODY,
        "type": "show",
        "content": {
            "shows": [
                {
                    "location": {
                        "venue": "Melbourne Cricket Ground",
                        "address": "Brunton Ave, Richmond VIC 3002",
                    },
                    "artist": "Frank Sinatra",
                    "date": "2022-03-27 12:00:00",
                    "sale": {
                        "status": sale_status,
                    }
                }
            ]
        }
    })
    assert response.status_code == 201




def test_get_by_link_id() -> None:
    create_response = requests.post(f"{API_BASE_URL}/{LINK_ROUTE}", json=VALID_POST_LINK_BODY)
    assert create_response.status_code == 201
    link_id = create_response.json()["id"]

    get_response = requests.get(f"{API_BASE_URL}/{LINK_ROUTE}/{link_id}")

    assert get_response.status_code == 200


def test_list_links() -> None:
    create_response = requests.post(f"{API_BASE_URL}/{LINK_ROUTE}", json=VALID_POST_LINK_BODY)
    assert create_response.status_code == 201

    get_response = requests.get(f"{API_BASE_URL}/{LINK_ROUTE}")

    assert get_response.status_code == 200
    assert len(get_response.json()["items"]) > 1


def test_list_links_with_user_id_query_param() -> None:
    user_id = str(uuid4())
    create_response = requests.post(f"{API_BASE_URL}/{LINK_ROUTE}", json={
        **VALID_POST_LINK_BODY,
        "user_id": user_id
    })
    assert create_response.status_code == 201

    get_response = requests.get(f"{API_BASE_URL}/{LINK_ROUTE}?user_id={user_id}")

    assert get_response.status_code == 200
    assert len(get_response.json()["items"]) == 1
    assert get_response.json()["items"][0]["user_id"] == user_id


def test_list_links_with_sort_by_query_param() -> None:
    create_response_a = requests.post(f"{API_BASE_URL}/{LINK_ROUTE}", json=VALID_POST_LINK_BODY)
    assert create_response_a.status_code == 201

    create_response_b = requests.post(f"{API_BASE_URL}/{LINK_ROUTE}", json=VALID_POST_LINK_BODY)
    assert create_response_b.status_code == 201

    get_response = requests.get(f"{API_BASE_URL}/{LINK_ROUTE}?sort_by=created_at")
    assert get_response.status_code == 200

    response_created_at_ordered = [l["created_at"] for l in get_response.json()["items"]]
    assert response_created_at_ordered == sorted(response_created_at_ordered, key=lambda x: datetime.fromisoformat(x), reverse=True)


###################################################################################################
# Rainy Tests
###################################################################################################


def test_get_by_invalid_link_id_yeilds_404() -> None:
    get_response = requests.get(f"{API_BASE_URL}/{LINK_ROUTE}/{str(uuid4())}")

    assert get_response.status_code == 404
    assert get_response.json()["title"] == "Not Found"
