from pathlib import Path
from sys import path
from typing import Final

from git import Repo

repo: Final[Repo] = Repo(".", search_parent_directories=True)
PROJECT_ROOT: Path
if project_root_dir := repo.git.rev_parse("--show-superproject-working-tree"):
    PROJECT_ROOT = Path(project_root_dir)
else:
    PROJECT_ROOT = Path(repo.working_tree_dir)

# Add the paths we'll need to run tests.
path.append(str(PROJECT_ROOT.joinpath("src")))