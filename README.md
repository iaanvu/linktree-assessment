# Getting Started


- Step 1: Install python 3.10.0 for your machine.

https://www.python.org/downloads/release/python-3100/

Note. Setting up & activating python virtual environment is recommended.

- Step 2: Install pip dependencies.
```
pip install -r requirements.txt
```


- Step 3: Start application server.
```
cd src/http/
```
```
gunicorn --config gunicorn.conf.py app
```


# Test

Currently, there are two test suites: `unit` & `integration`.

To run `unit` test:
```
pytest test/unit
```

To run `integration` test, ensure application is runnning (refer above):
```
pytest test/integration
```

# API Documentation

The openapi spec (swagger) can be found under `src/http/openapi-spec.yaml`.
If you don't use any swagger plugins for your IDE you can copy and paste the yaml content into https://editor.swagger.io/ to view.



# Architecture

This project applies Domain Driven Design.

The backend application is split into 2 key layers. The `domain` layer and the `http` layer

One key constraint applied to the system is for the domain layer to not depend on the http layer. Only the http layer should depend on domain.

One advantage of this is the flexibility to pivot to any other type of interface. eg. GraphQL.


![High Level Architecture Diagram](./docs/high-level-architecture.jpg)


# The Problem
We have three new link types for our users.

1. Classic
	- Titles can be no longer than 144 characters.
	- Some URLs will contain query parameters, some will not.
2. Shows List
	- One show will be sold out.
	- One show is not yet on sale.
	- The rest of the shows are on sale.
3. Music Player
	- Clients will need to link off to each individual platform.
	- Clients will embed audio players from each individual platform.
	
You are required to create a JSON API that our front end clients will interact with.

- The API can be GraphQL or REST.
- The API can be written in your preferred language.
- The client must be able to create a new link of each type.
- The client must be able to find all links matching a particular userId.
- The client must be able to find links matching a particular userId, sorted by dateCreated.


## Your Solution

- Consider bad input data and the end user of your API - we're looking for good error handling and input validation.
- If you are creating a GraphQL API, think about the access patterns the client may use, and think about the acces patterns the client may not use. Try not to [Yak Shave](https://seths.blog/2005/03/dont_shave_that/)
- Consider extensibility, these are 3 of hundreds of potential link types that we will be developing.


## Rules & Tips

- Choose the language and environment of your choice, just include documentation on how to run your code.
- Immutability and functional programming is looked upon favorably.
- You cannot connect to a real world database - document your schema design.
- Mocking third parties is looked upon favorably.
- @todo comments are encouraged. You aren't expected to complete the challenge, but how you design your solution and your ideas for the future are important.

---
# Submission
Set up your own remote git repository and make commits as you would in your day to day work. Submit a link to your repo when you're finished.
